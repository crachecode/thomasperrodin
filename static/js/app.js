import { getCookie, setCookie } from './cookie.js'

let cookieTheme = getCookie('theme')
let cookieWidth = getCookie('width')
let cookieMaxAge = 365*24*60*60

function hideMenu() {
  document.querySelector('body > header').classList.remove('opened')
}

document.addEventListener('DOMContentLoaded', () => {
  document.querySelector('#sandwich').addEventListener('click', () => {
    document.querySelector('body > header').classList.toggle('opened')
  })
  document.querySelectorAll('header .switcher, header nav a').forEach( (item) => {
    item.addEventListener('click', () => {
      hideMenu()
    })
  })
})
