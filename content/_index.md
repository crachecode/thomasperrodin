---
title: Accueil
id: index
draft: false
---

Bienvenue sur le site de Thomas Perrodin, graphiste, sérigraphe et illustrateur de talent, électron libre hyper actif de la scène Genevoise depuis bientôt 15 ans !
