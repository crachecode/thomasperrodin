# Pile [Jamstack](https://jamstatic.fr/2019/02/07/c-est-quoi-la-jamstack/) pour la gestion du site [www.thomasperrodin.com](https://www.thomasperrodin.com)

basé sur [Hugo](https://gohugo.io/), [Netlify CMS](https://www.netlifycms.org) et [GitLab](https://gitlab.com).

## utilisation

- front-end : https://crachecode.gitlab.io/thomasperrodin
- gestion du contenu : https://crachecode.gitlab.io/thomasperrodin/admin/

## gestion des accès

L'accès à la gestion du contenu se fait au moyen d'un compte [GitLab](https://gitlab.com/users/sign_in).  
L'utilisateur doit avoir au minimum un rôle _Maintainer_ sur le projet [thomasperrodin](https://gitlab.com/crachecode/tomasperrodin/-/project_members).
